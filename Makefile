# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/09/09 12:59:16 by tlovato           #+#    #+#              #
#    Updated: 2017/01/22 17:45:14 by tlovato          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = 21sh
INC_DIR = includes
SRCS_DIR = srcs/

SRCS_FILES = main.c lexpa_parser.c frees.c env_get_path.c init.c redirs.c\
env_change_env.c lexpa_split_cmd.c builtin_cd.c builtin_cd_utils.c builtin_exit.c\
command_parsing.c command_exec.c builtin_echo.c builtin_env.c builtin_setenv.c\
builtin_unsetenv.c pipes.c ledit_move_cursor.c redir_heredoc.c redir_out.c redir_in.c\
ledit_car_del.c ledit_get_line.c ledit_car_add.c ledit_history.c ledit_ccp.c\
ledit_paste.c ledit_move_curword.c ledit_move_curline.c signals.c\
redir_agreg_fd.c env_create_env.c lexpa_spe_car_substitution.c builtin_cd_options.c\
builtin_cd_construct_path.c builtin_env_utils.c ledit_car_action.c\
ledit_move_cursor_right.c ledit_move_cursor_left.c lexpa_get_cmd.c lexpa_get_args.c\
lexpa_var_sub.c lexpa_bquotes_sub.c ledit_get_line_quotes.c lexpa_get_tokens.c\
lexpa_split_cmd_counts.c lexpa_get_redirs.c\

SRCS = $(addprefix $(SRCS_DIR), $(SRCS_FILES))

OBJS = $(SRCS:.c=.o)

all: $(NAME)

%.o: %.c
		@gcc -g3 -o $@ -c $<

$(NAME): $(OBJS)
					@echo "\033[1;32m Objects done\033[m"
					@make -C libft/
					@gcc -o $@ $^ -L libft/ -lft -lncurses 
					@echo "\033[1;32m 21sh done\033[m"

clean:
					@rm -f $(OBJS)
					@echo "\033[1;31m Cleaning done\033[m"

fclean: clean
					@rm -f $(NAME)
					@cd libft && make fclean
					@echo "\033[1;31m Fcleaning done\033[m"

re: fclean all

.PHONY: all clean fclean re
