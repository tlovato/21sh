/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 11:14:56 by tlovato           #+#    #+#             */
/*   Updated: 2015/12/14 13:29:40 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *str, int c, size_t n)
{
	size_t				i;
	unsigned char		car;
	unsigned char		*s;

	i = 0;
	car = (unsigned char)c;
	s = (unsigned char *)str;
	while (i < n)
	{
		if (s[i] == car)
			return (&s[i]);
		i++;
	}
	return (NULL);
}
