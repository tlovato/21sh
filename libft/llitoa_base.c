/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   llitoa_base.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 23:19:18 by tlovato           #+#    #+#             */
/*   Updated: 2016/05/25 23:19:21 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static long long		ft_intlen(long long n, int base)
{
	long long			len;

	len = 0;
	if (n == 0)
		return (1);
	while (n > 0)
	{
		n = n / base;
		len++;
	}
	return (len);
}

static char				*ft_core(long long n, char *str, int base)
{
	long long			len;
	char				*tab;

	tab = "0123456789ABCDEF";
	len = ft_intlen(n, base);
	str[len] = '\0';
	while (n != 0)
	{
		str[--len] = tab[n % base];
		n = n / base;
	}
	return (str);
}

char					*ft_llitoa_base(long long nb, int base)
{
	char				*str;
	long long			i;
	long long			n;

	str = (char *)malloc(ft_intlen(nb, base) + 1 * (sizeof(char)));
	if (str == NULL)
		return (NULL);
	if (base < 2 || base > 16)
		return (NULL);
	i = 0;
	n = nb;
	if (n == 0)
	{
		str[0] = '0';
		str[1] = '\0';
		return (str);
	}
	return (ft_core(n, str, base));
}
