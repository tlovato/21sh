/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexpa_get_cmd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 15:17:14 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/23 15:17:21 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static void		init_new(t_cmd **new)
{
	*new = (t_cmd *)malloc(sizeof(t_cmd));
	(*new)->redir = NULL;
	(*new)->args = NULL;
	(*new)->next = NULL;
}

static int		count_redir(char **tmp)
{
	int			i;
	int			redir;

	i = 0;
	redir = 0;
	while (tmp[i])
	{
		if (ft_strchr(tmp[i], '>') || ft_strchr(tmp[i], '<')
			|| !ft_strcmp(tmp[i], "<<") || !ft_strcmp(tmp[i], ">>"))
			redir++;
		i++;
	}
	return (redir);
}

static void		get_vars(t_cmd *new, char **tmp)
{
	int			max;

	max = count_redir(tmp);
	new->redir = get_redir(tmp, 0, max, 0);
	new->args = get_args(tmp);
}

static char		*find_next(char *token, char *cmd)
{
	char		*tmp;
	int			i;

	i = 0;
	tmp = NULL;
	if (cmd && token)
	{
		tmp = ft_strstr(token, cmd);
		if (tmp)
			while (tmp[i] && tmp[i] != '|')
				i++;
	}
	if (!tmp)
		return (NULL);
	return ((i >= (int)ft_strlen(tmp)) ? NULL : &tmp[i]);
}

t_cmd			*get_cmd(char *token, int max, int i, t_all *all)
{
	t_cmd		*new;
	char		**tmp;
	char		**tmpt;
	char		*next;

	if (i < max)
	{
		init_new(&new);
		tmpt = ft_strsplit(token, '|');
		tmp = split_cmd(tmpt[0], all);
		if (tmp)
		{
			next = find_next(token, tmp[0]);
			new->pipe = (next) ? 1 : 0;
			get_vars(new, tmp);
			i += 1;
			ft_free_str_tab(tmpt);
			ft_free_str_tab(tmp);
			new->next = get_cmd(next, max, i, all);
		}
		return (new);
	}
	return (NULL);
}
