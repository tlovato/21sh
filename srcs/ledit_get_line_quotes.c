/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_line_quotes.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/30 16:56:12 by tlovato           #+#    #+#             */
/*   Updated: 2017/03/03 15:46:06 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

void		handle_quotes(t_line *line, int *qbuf, int buf)
{
	if (!line->quotes && *qbuf)
		*qbuf = 0;
	if (!*qbuf)
		*qbuf = buf;
	if (line->quotes && buf == *qbuf)
		line->quotes = 0;
	else
		line->quotes = 1;
}

void		val_cur_quotes(t_line *line)
{
	line->cur += 3;
	line->curinline = 0;
	line->nlines++;
	line->line++;
	ft_putstr("> ");
}
