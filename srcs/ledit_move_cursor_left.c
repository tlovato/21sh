/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ledit_move_cursor_left.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 15:03:55 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/23 15:04:07 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static void		deselect_car(t_line *line, char *newstock)
{
	char		*tmp;

	tmp = ft_strdup(&newstock[line->cur]);
	ft_putchar(127);
	ft_putstr("\033[1D");
	ft_putstr("\033[0m");
	ft_putchar(newstock[line->cur + 1]);
	ft_putstr("\033[1D");
	line->cpy[ft_strlen(line->cpy) - 1] = '\0';
}

static void		else_stuff(t_line *line)
{
	if (line->newstock[line->cur] == ' '
		&& line->newstock[line->cur - 1] == ' '
		&& line->newstock[line->cur - 2] == ' '
		&& line->newstock[line->cur - 3] == ' ')
	{
		ft_putstr("\033[4D");
		line->cur -= 3;
		line->curinline -= 4;
	}
	else
	{
		ft_putstr("\033[1D");
		line->curinline -= 1;
	}
}

void			cur_left(t_line *line, char *newstock)
{
	if (line->cur >= 1)
	{
		line->cur -= 1;
		if (line->ccp)
		{
			if (line->cur >= line->cutstart)
				deselect_car(line, newstock);
			else
				return ;
		}
		if (line->curinline == 0)
		{
			ft_putstr("\033[1A");
			line->line -= 1;
			while (line->curinline < (line->ncol - 1))
			{
				ft_putstr("\033[1C");
				line->curinline++;
			}
		}
		else
			else_stuff(line);
	}
}
