/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_cd_construct_path.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 12:12:10 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/23 12:12:13 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

char		*cd_construct_path(char *av)
{
	char	*pwd;
	char	*tmp;
	char	*ret;
	int		ok;

	ret = NULL;
	if (*av == '/')
		return (ft_strdup(av));
	if (!(pwd = ft_strnew(PATH_MAX)))
		return (NULL);
	if (!(pwd = getcwd(pwd, PATH_MAX)))
		return (NULL);
	ft_strcat(pwd, "/");
	tmp = ft_strjoin(pwd, av);
	ok = (!access(tmp, X_OK));
	if (ok)
		ret = ft_strdup(tmp);
	free(pwd);
	free(tmp);
	return ((ok) ? ret : NULL);
}
