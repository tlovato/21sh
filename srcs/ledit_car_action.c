/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ledit_get_line_utils.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 14:14:03 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/23 14:14:17 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static void		print_car(char *buf, char **newstock, t_line *line)
{
	char		*cat;

	cat = ft_strnew(1);
	write(1, buf, ft_strlen(buf));
	ft_strcpy(cat, (char *)buf);
	*newstock = ft_strjoinfree(*newstock, cat, 3);
	line->right_margin = ft_strlen(*newstock) + 3;
	if (line->tabu)
	{
		line->cur += 4;
		line->curinline += 4;
	}
	else
	{
		line->cur += 1;
		line->curinline += 1;
	}
}

void			pars_car_action(char *buf, char **newstock, t_line *line)
{
	if (buf[0] != 127)
	{
		if (buf[0] == '\t')
		{
			line->tabu = 1;
			ft_bzero(buf, 1);
			buf = ft_strdup("    ");
		}
		else
		{
			line->tabu = 0;
			buf[1] = '\0';
		}
		if (line->cur < (int)ft_strlen(*newstock))
			add_car(buf, newstock, line);
		else
			print_car(buf, newstock, line);
	}
	else
		del_car(line, newstock);
	if (!ft_strcmp(buf, "    "))
		free(buf);
}
