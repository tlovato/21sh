/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_cd_utils.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/14 13:05:01 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/22 13:59:49 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

void			ft_cd_errors(int err, char *arg)
{
	if (err == 1)
	{
		ft_putstr("21sh: cd: String not in pwd: ");
		ft_putendl(arg);
	}
	else if (err == 2)
	{
		ft_putstr("21sh: cd: No such file or diectory: ");
		ft_putendl(arg);
	}
}

char			*cd_find_var(char *tofind, t_all *all)
{
	char		*var;
	t_lstenv	*tmp;

	var = NULL;
	tmp = all->env->lstenv;
	tofind = ft_strjoin(tofind, "=");
	while (tmp)
	{
		if (!ft_strncmp(tofind, tmp->line, ft_strlen(tofind)))
			var = ft_strsub(tmp->line, 0, ft_strlen(tmp->line));
		tmp = tmp->next;
	}
	free(tofind);
	return (var);
}

void			cd_change_vars(char *tochange, char *newval, t_all *all)
{
	t_lstenv	*tmp;
	char		*sub;
	int			len;

	len = ft_strlen(tochange);
	tmp = all->env->lstenv;
	while (tmp)
	{
		sub = ft_strsub(tmp->line, 0, ft_strlen(tochange));
		if (!ft_strcmp(sub, tochange))
		{
			ft_bzero(&tmp->line[len], ft_strlen(tmp->line) - len);
			tmp->line = ft_strjoinfree(tmp->line, newval, 1);
		}
		free(sub);
		tmp = tmp->next;
	}
}

void			cd_free_str(char *s1, char *s2, char *s3)
{
	if (s1)
		free(s1);
	if (s2)
		free(s2);
	if (s3)
		free(s3);
}
