/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_ft_exit.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/14 11:45:53 by tlovato           #+#    #+#             */
/*   Updated: 2016/10/14 11:45:55 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

void		ft_exit(char **args, t_all *all)
{
	if (!args[1])
	{
		ft_free_str_tab(all->env->tabenv);
		exit(0);
	}
	else if (args[1] && !args[2])
	{
		ft_free_str_tab(all->env->tabenv);
		exit(ft_atoi(args[1]));
	}
	else
		ft_printf("21sh: exit: Too many arguments\n");
}
