/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redirs.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/18 14:42:23 by tlovato           #+#    #+#             */
/*   Updated: 2017/03/18 14:42:24 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static void		for_redirs(t_redir *redir, t_cmd *cmd, t_all *all)
{
	if (!redir->file_name)
	{
		ft_putendl("21sh: syntax error");
		exit(EXIT_FAILURE);
	}
	else
	{
		if (!ft_strcmp(redir->redir, ">")
			|| !ft_strcmp(redir->redir, ">>"))
			redir_out(cmd, redir, all);
		if (!ft_strcmp(redir->redir, "<"))
			redir_in(cmd, redir, all);
		if (!ft_strcmp(redir->redir, "<<"))
			redir_heredoc(cmd, all);
	}
}

void			pars_redir(t_cmd *cmd, t_redir *redir, t_all *all)
{
	while (redir)
	{
		if (redir->fd1 == -1)
			for_redirs(redir, cmd, all);
		else if (!redir->file_name)
		{
			if (redir->fd2 == -1)
				close(redir->fd1);
			else if (redir->fd2 != -1)
				dup2(redir->fd2, redir->fd1);
			else
				ft_putendl("21sh : syntax error");
		}
		redir = redir->next;
	}
}
