/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexpa_bquotes.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 16:14:59 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/23 16:15:07 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static char		*bquotes_sub(char *tmpline, int q, char **start, char **end)
{
	int			i;
	int			st;

	i = 0;
	st = 0;
	while (tmpline[i])
	{
		if (tmpline[i] == '`')
		{
			*start = ft_strsub(tmpline, 0, i);
			i++;
			st = i;
			while (tmpline[i] && (tmpline[i] != '`'))
				i++;
			*end = ft_strsub(tmpline, i + 1, ft_strlen(tmpline) - i);
			return (ft_strsub(tmpline, st, i - st));
		}
		i++;
	}
	return ((q == '`') ? ft_strdup(tmpline) : NULL);
}

static char		*read_new(int fd)
{
	char		buf[2];
	char		*cat;
	char		*new;

	new = ft_strnew(1);
	while (read(fd, buf, 1))
	{
		cat = ft_strnew(1);
		buf[1] = '\0';
		ft_strcpy(cat, buf);
		new = ft_strjoinfree(new, cat, 3);
	}
	return (new);
}

static char		*pipe_and_fork(t_cmd *cmd, t_all *all)
{
	int			pipefd[2];
	pid_t		pid;
	char		*new;

	pipe(pipefd);
	pid = fork();
	if (!pid)
	{
		close(pipefd[0]);
		dup2(pipefd[1], STDOUT_FILENO);
		pars_command(cmd, cmd->args, all);
		exit(EXIT_SUCCESS);
	}
	else
	{
		wait(NULL);
		close(pipefd[1]);
		new = read_new(pipefd[0]);
	}
	return (new);
}

static char		*join_tmpline(char **new, char *start, char *end, char *tmpline)
{
	if (start)
		tmpline = ft_strjoinfree(start, *new, 3);
	if (end)
		tmpline = ft_strjoinfree(tmpline, end, 3);
	if (!start && !end)
	{
		tmpline = ft_strdup(*new);
		free(*new);
	}
	return (tmpline);
}

char			*bquotes(char *tmpline, t_all *all, int quotes)
{
	t_cmd		*cmd;
	char		*bquotes_cmd;
	char		*start;
	char		*end;
	char		*new;

	cmd = NULL;
	start = NULL;
	end = NULL;
	bquotes_cmd = bquotes_sub(tmpline, quotes, &start, &end);
	if (bquotes_cmd)
	{
		cmd = get_cmd(bquotes_cmd, count_cmds(bquotes_cmd), 0, all);
		all->env->ntabenv = cpy_env(all->env->tabenv);
		new = pipe_and_fork(cmd, all);
		free(bquotes_cmd);
		free(tmpline);
		ft_free_str_tab(all->env->ntabenv);
		new[ft_strlen(new) - 1] = '\0';
		tmpline = join_tmpline(&new, start, end, tmpline);
		free_cmd(cmd);
	}
	return (tmpline);
}
