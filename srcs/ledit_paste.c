/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ledit_paste.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 14:40:50 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/10 12:52:21 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static void		replace_cur(t_line *line)
{
	int			car;

	car = 0;
	while (car < (int)ft_strlen(line->cpy))
	{
		car++;
		line->cur++;
		line->curinline++;
		if (line->curinline == line->ncol)
		{
			line->line++;
			line->curinline = 0;
		}
	}
}

void			paste_cpy(t_line *line, char **newstock)
{
	char		*tmp;
	char		*tmp1;
	char		*tmp2;

	tmp = *newstock;
	tmp1 = ft_strnew(line->cur);
	ft_strncpy(tmp1, *newstock, line->cur);
	tmp2 = ft_strdup(&tmp[line->cur]);
	free(*newstock);
	*newstock = ft_strjoinfree(tmp1, line->cpy, 1);
	*newstock = ft_strjoinfree(*newstock, tmp2, 1);
	tputs(tgetstr("ce", NULL), 1, ft_puts);
	ft_putstr_fd(line->cpy, 1);
	replace_cur(line);
	ft_putstr_fd(tmp2, 1);
	cur_right(line, tmp2);
	free(tmp2);
}
