/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexpa_split_cmd_counts.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/17 15:34:17 by tlovato           #+#    #+#             */
/*   Updated: 2017/03/17 15:34:18 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

int				count_words(char *line)
{
	int			val[3];

	val[0] = 0;
	val[1] = 0;
	val[2] = 0;
	while (line[val[0]] && (line[val[0]] != '|'))
	{
		if (line[val[0]] != ' ' && line[val[0]] != '\t')
			val[1]++;
		if (line[val[0]] == '"' || line[val[0]] == '\'' || line[val[0]] == '`')
		{
			val[2] = line[val[0]];
			val[0]++;
			while ((line[val[0]]) && line[val[0]] != val[2])
				val[0]++;
		}
		while ((line[val[0]] && line[val[0]] != '|')
			&& (line[val[0]] != ' ' && line[val[0]] != '\t'))
			val[0]++;
		if (line[val[0]] == '\0')
			return (val[1]);
		val[0]++;
	}
	return (val[1]);
}

static void		new_values(int *val, int *k, int *quotes, char **line)
{
	char		*start;
	char		*end;
	int			len;

	start = NULL;
	end = NULL;
	len = val[0] + 1;
	val[2] = 1;
	val[1] = (*line)[val[0]];
	if (val[0])
		start = ft_strsub(*line, 0, val[0]);
	while ((*line)[len] && (*line)[len] != val[1])
		len++;
	end = ft_strsub(*line, val[0] + 1, len - 1);
	*line = (start) ? ft_strjoinfree(start, end, 3) : end;
	if (!start)
		free(end);
	*k = *k + 1;
	*quotes = val[1];
}

int				count_wlen(char **line, int *k, int *quotes)
{
	int			val[3];

	val[0] = 0;
	val[1] = 0;
	val[2] = -1;
	while ((*line)[val[0]])
	{
		if (!(*quotes) && ((*line)[val[0]] == '"'
			|| (*line)[val[0]] == '\'' || (*line)[val[0]] == '`'))
			new_values(val, k, quotes, line);
		else if (!val[1])
		{
			if ((*line)[val[0]] != ' ' && (*line)[val[0]] != '\t')
				val[2] = (val[2] != -1) ? val[2] : val[0];
			else
				return (val[0] - val[2]);
		}
		val[0]++;
	}
	return (val[0]);
}
