/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexpa_get_args.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 15:32:34 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/23 15:32:47 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static int		calc_len(char **tmp)
{
	int			i;
	int			len;

	i = 0;
	len = ft_strtablen(tmp);
	while (tmp[i])
	{
		if (ft_strchr(tmp[i], '>') || ft_strchr(tmp[i], '<')
			|| !ft_strcmp(tmp[i], "<<") || !ft_strcmp(tmp[i], ">>"))
			len = (tmp[i + 1]) ? len - 2 : len - 1;
		i++;
	}
	return (len);
}

char			**get_args(char **tmp)
{
	int			len;
	int			i;
	char		**ret;

	i = 0;
	len = 0;
	ret = (char **)malloc(sizeof(char *) * (calc_len(tmp) + 1));
	while (tmp[i])
	{
		if ((i >= 0) && (ft_strchr(tmp[i], '>') || ft_strchr(tmp[i], '<')))
			i = (tmp[i + 1]) ? i + 2 : i + 1;
		if (tmp[i] && (!ft_strchr(tmp[i], '>') && !ft_strchr(tmp[i], '<')))
		{
			ret[len] = ft_strdup(tmp[i]);
			i++;
			len++;
		}
	}
	ret[len] = NULL;
	return (ret);
}
