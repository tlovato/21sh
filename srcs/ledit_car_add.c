/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ledit_car_add.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/14 18:03:12 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/14 12:18:28 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static void		display_new_car(char *buf, char *tmp2)
{
	int			car;

	car = 0;
	tputs(tgetstr("ce", NULL), 1, ft_puts);
	write(1, buf, 1);
	ft_putstr_fd(tmp2, 1);
	while (car < (int)ft_strlen(tmp2))
	{
		ft_putstr("\033[1D");
		car++;
	}
}

static void		place_cur(t_line *line)
{
	line->cur += 1;
	if (line->curinline >= (line->ncol - 1))
	{
		line->line += 1;
		line->curinline = 0;
	}
	else
		line->curinline += 1;
}

void			add_car(char *buf, char **newstock, t_line *line)
{
	char		*tmp1;
	char		*tmp2;
	char		*tmp;
	char		*cat;

	tmp = *newstock;
	cat = ft_strnew(1);
	buf[1] = '\0';
	ft_strcpy(cat, (char *)buf);
	tmp1 = (char *)malloc(sizeof(char) * (line->cur + 1));
	ft_strncpy(tmp1, *newstock, line->cur);
	tmp1[line->cur] = '\0';
	tmp2 = ft_strdup(&tmp[line->cur]);
	free(*newstock);
	*newstock = ft_strjoinfree(tmp1, cat, 3);
	*newstock = ft_strjoinfree(*newstock, tmp2, 1);
	display_new_car(buf, tmp2);
	free(tmp2);
	place_cur(line);
}
