/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexpa_get_token.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/17 15:29:59 by tlovato           #+#    #+#             */
/*   Updated: 2017/03/17 15:30:03 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

int				count_cmds(char *token)
{
	int			i;
	int			cmds;

	i = 0;
	cmds = 1;
	while (token[i])
	{
		if (token[i] == '|')
			cmds++;
		i++;
	}
	return (cmds);
}

t_token			*get_tokens(char **seps, int i, t_all *all)
{
	t_token		*new;
	int			cmds;

	if (i < ft_strtablen(seps))
	{
		new = (t_token *)malloc(sizeof(t_token));
		cmds = count_cmds(seps[i]);
		new->cmd = get_cmd(seps[i], cmds, 0, all);
		i += 1;
		new->next = get_tokens(seps, i, all);
		return (new);
	}
	return (NULL);
}
