/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexpa_var_sub.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 16:05:44 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/23 16:05:53 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static char		*join_start(char *tmpline, int start, int i)
{
	char		*sub;
	char		*env;

	sub = NULL;
	env = NULL;
	sub = ft_strsub(tmpline, start + 1, i - start);
	env = getenv(sub);
	free(sub);
	return (ft_strjoinfree(ft_strsub(tmpline, 0, start), env ? env : " ", 1));
}

char			*var_substitution(char *l)
{
	int			i;
	int			start;
	char		*tmp;
	char		*end;

	i = 0;
	start = 0;
	while (l[i])
	{
		if (l[i] == '$')
		{
			start = i;
			while (l[i] && (l[i] != ' ' && l[i] != '\t'))
				i++;
			if (i != ft_strlen(l))
				i -= 1;
			end = ft_strsub(l, (i != ft_strlen(l)) ? i + 1 : i, ft_strlen(l));
			free(l);
			l = ft_strjoinfree(join_start(l, start, i), end, 3);
		}
		i++;
	}
	return (l);
}
