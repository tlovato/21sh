/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command_parsing.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/12 10:59:48 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/19 18:04:02 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static int		valid_builtin(char *command)
{
	return (!ft_strcmp(command, "setenv") || !ft_strcmp(command, "unsetenv") ||
		!ft_strcmp(command, "env") || !ft_strcmp(command, "echo")
		|| !ft_strcmp(command, "exit") || !ft_strcmp(command, "cd"));
}

static void		pars_builtin(t_cmd *cmd, char **args, t_all *all)
{
	if (!ft_strcmp(args[0], "setenv"))
		ft_setenv(args, all);
	if (!ft_strcmp(args[0], "unsetenv"))
		ft_unsetenv(args, all);
	if (!ft_strcmp(args[0], "env"))
		ft_env(cmd, args, all);
	if (!ft_strcmp(args[0], "echo"))
		ft_echo(args, all);
	if (!ft_strcmp(args[0], "exit"))
		ft_exit(args, all);
	if (!ft_strcmp(args[0], "cd"))
		ft_cd(args, all);
}

void			do_exec(t_cmd *cmd, char **args, t_all *all)
{
	if (cmd->redir
		&& (!cmd->pipe || (cmd->pipe && cmd->redir->fd1 != -1)))
		pars_redir(cmd, cmd->redir, all);
	if (valid_builtin(args[0]))
	{
		pars_builtin(cmd, cmd->args, all);
		exit(EXIT_SUCCESS);
	}
	if (!valid_builtin(args[0]))
		execution(args, all, (args[0][0] != '/'));
}

void			pars_command(t_cmd *cmd, char **args, t_all *all)
{
	all->ongoing = 1;
	if (valid_builtin(args[0]) && !cmd->pipe && !cmd->redir)
		pars_builtin(cmd, cmd->args, all);
	else
	{
		all->pid = fork();
		if (!all->pid)
		{
			if (cmd->redir && cmd->pipe && !ft_strcmp(cmd->redir->redir, "<<"))
				pars_redir(cmd, cmd->redir, all);
			if (cmd->pipe && valid(cmd, all))
				communication(cmd, all);
			do_exec(cmd, args, all);
			exit(EXIT_FAILURE);
		}
		wait(NULL);
	}
}
