/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/11 17:55:20 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/17 15:24:20 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

void			init_envs(t_env **envs, char **env)
{
	*envs = (t_env *)malloc(sizeof(t_env));
	if (!env[0])
		create_env(envs);
	else
	{
		(*envs)->lstenv = get_env_lst(env, 0);
		(*envs)->tabenv = cpy_env(env);
	}
}

void			init_shell(t_all *all)
{
	char			*tmp;

	ft_putstr("$> ");
	all->heredoc = -1;
	all->redir = 0;
	all->line.ccp = 0;
	all->line.cpy = NULL;
	all->line.left_margin = 3;
	all->line.ncol = tgetnum("co");
	all->line.cutstart = 0;
	all->tmpctrlhist = all->ctrlhist;
	tmp = get_line(&all->line, &all->tmpctrlhist, all);
	add_to_history(tmp, &all->ctrlhist);
	all->token = parser(tmp, &all->token, all);
	free(tmp);
}

int				init_term(t_all *all)
{
	char		*tmp;

	tmp = ttyname(STDIN_FILENO);
	if ((all->fd = open(tmp, O_WRONLY)) < 0)
		return (0);
	if ((tgetent(NULL, getenv("TERM")) == ERR))
		return (0);
	if (tcgetattr(all->fd, &all->term) == ERR
		|| tcgetattr(all->fd, &all->init) == ERR)
		return (0);
	all->ctrlhist.head = NULL;
	all->ctrlhist.tail = NULL;
	all->ctrlhist.len = 0;
	free(tmp);
	return (1);
}
