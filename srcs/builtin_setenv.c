/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_setenv.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/12 15:49:22 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/22 15:43:04 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static void		modif_var(char *arg, t_lstenv **lstenv)
{
	t_lstenv	*tmp;
	char		*sub;
	int			equline;
	int			equarg;

	tmp = *lstenv;
	if (!find_equal(arg))
		arg = ft_strjoin(arg, "=");
	equarg = find_equal(arg);
	while (tmp)
	{
		equline = find_equal(tmp->line);
		sub = ft_strsub(tmp->line, 0, equline + 1);
		if (!(ft_strncmp(sub, arg, equarg)))
		{
			ft_bzero(&tmp->line[equline], ft_strlen(tmp->line) - equline);
			tmp->line = ft_strjoinfree(tmp->line, &arg[equarg], 1);
		}
		free(sub);
		tmp = tmp->next;
	}
}

static void		add_var(char *arg, t_lstenv **lstenv)
{
	t_lstenv	*new;
	t_lstenv	*tmp;

	tmp = *lstenv;
	new = (t_lstenv *)malloc(sizeof(t_lstenv));
	new->line = ft_strdup(arg);
	if (!find_equal(new->line))
		new->line = ft_strjoinfree(new->line, "=", 1);
	new->next = NULL;
	if (tmp)
	{
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
	}
	else
		*lstenv = new;
}

static int		find_var(char **arg, t_all *all)
{
	t_lstenv	*tmp;
	int			len;

	tmp = all->env->lstenv;
	len = find_equal(*arg);
	if (!len)
		*arg = ft_strjoinfree(*arg, "=", 1);
	while (tmp)
	{
		if (!ft_strncmp(*arg, tmp->line, len ? (len + 1) : ft_strlen(*arg)))
			return (1);
		tmp = tmp->next;
	}
	return (0);
}

static int		valid_arg_setenv(char *arg)
{
	int			i;
	int			len;

	i = 0;
	len = find_equal(arg);
	if (!len)
		len = ft_strlen(arg);
	while (i < len)
	{
		if (!ft_isalnum(arg[i]) && arg[i] != '=')
			return (0);
		i++;
	}
	return (1);
}

void			ft_setenv(char **args, t_all *all)
{
	int			i;

	i = 1;
	if (ft_strtablen(args) == 1)
		display_env(all);
	while (args[i])
	{
		if (!valid_arg_setenv(args[i]))
		{
			ft_putstr("21sh: setenv: Variable name must contain");
			ft_putendl(" alphanumeric caracters\n");
		}
		else
		{
			if (!find_var(&args[i], all))
				add_var(args[i], &all->env->lstenv);
			else
				modif_var(args[i], &all->env->lstenv);
		}
		i++;
	}
}
