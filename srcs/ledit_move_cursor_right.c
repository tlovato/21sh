/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ledit_move_cursor_right.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 15:02:06 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/23 15:02:14 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static void		select_car(t_line *line, char *newstock)
{
	char		*tmp;

	tmp = ft_strsub(newstock, line->cur - 1, 1);
	ft_putchar(127);
	ft_putstr("\033[1D");
	ft_putstr("\033[7m");
	ft_putstr(tmp);
	ft_putstr("\033[0m");
	ft_putstr("\033[1D");
	line->cpy = ft_strjoinfree(line->cpy, tmp, 3);
}

static void		else_stuff(t_line *line)
{
	if (line->newstock[line->cur - 1] == ' '
		&& line->newstock[line->cur] == ' '
		&& line->newstock[line->cur + 1] == ' '
		&& line->newstock[line->cur + 2] == ' ')
	{
		ft_putstr("\033[4C");
		line->cur += 3;
		line->curinline += 4;
	}
	else
	{
		ft_putstr("\033[1C");
		line->curinline += 1;
	}
}

void			cur_right(t_line *line, char *newstock)
{
	if (line->cur < (int)(ft_strlen(newstock)))
	{
		line->cur += 1;
		if (line->ccp)
			select_car(line, newstock);
		if (line->curinline == (line->ncol - 1))
		{
			ft_putstr("\033[1B");
			line->line += 1;
			while (line->curinline > 0)
			{
				ft_putstr("\033[1D");
				line->curinline--;
			}
		}
		else
			else_stuff(line);
	}
}
