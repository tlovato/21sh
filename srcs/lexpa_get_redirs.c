/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexpa_get_redirs.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/17 15:38:54 by tlovato           #+#    #+#             */
/*   Updated: 2017/03/17 15:38:56 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static int		classic_redir(char *redir)
{
	return (!ft_strcmp(redir, ">") || !ft_strcmp(redir, "<")
		|| !ft_strcmp(redir, "<<") || !ft_strcmp(redir, ">>"));
}

static int		find_start(char **tmp, int start)
{
	int			add;

	add = 1;
	if (ft_strchr(tmp[start], '>') || ft_strchr(tmp[start], '<'))
	{
		add = 0;
		start++;
	}
	else if (!ft_strcmp(tmp[start], "<<") || !ft_strcmp(tmp[start], ">>"))
		start++;
	while (tmp[start] && ((!ft_strchr(tmp[start], '>')
		&& !ft_strchr(tmp[start], '<') && ft_strcmp(tmp[start], "<<")
		&& ft_strcmp(tmp[start], ">>"))))
		start++;
	return (start + add);
}

t_redir			*get_redir(char **tmp, int i, int max, int st)
{
	t_redir		*new;
	int			next_fd;

	next_fd = 0;
	if (i < max)
	{
		new = (t_redir *)malloc(sizeof(t_redir));
		st = (tmp[0][0] == '<' || tmp[0][0] == '>') ? 1 : find_start(tmp, st);
		new->redir = ft_strdup(tmp[st - 1]);
		if (classic_redir(new->redir))
		{
			new->file_name = (tmp[st]) ? ft_strdup(tmp[st]) : NULL;
			new->fd1 = -1;
		}
		else
		{
			new->file_name = NULL;
			new->fd1 = get_redir_fd(new->redir, &next_fd);
			new->fd2 = get_redir_fd(&new->redir[next_fd], &next_fd);
		}
		i += 1;
		new->next = get_redir(tmp, i, max, st);
		return (new);
	}
	return (NULL);
}
