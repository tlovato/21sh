/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_cd_options.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 12:00:31 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/23 12:00:43 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static void			change_vars(t_all *all, char **args, char *final_path)
{
	char			*oldpwd;
	char			*pwd;

	pwd = cd_find_var("PWD", all);
	oldpwd = cd_find_var("OLDPWD", all);
	if (pwd && oldpwd)
	{
		cd_change_vars("OLDPWD=", &pwd[4], all);
		if (!ft_strcmp(args[1], "-L"))
		{
			if (args[2][0] == '/')
				cd_change_vars("PWD=", final_path, all);
			else
			{
				pwd = ft_strjoinfree(pwd, "/", 1);
				pwd = ft_strjoinfree(pwd, args[2], 1);
				cd_change_vars("PWD=", &pwd[4], all);
			}
		}
		else
			cd_change_vars("PWD=", final_path, all);
	}
	cd_free_str(pwd, oldpwd, final_path);
}

void				case_option(char **args, t_all *all)
{
	struct stat		bufstruct;
	char			buf[PATH_MAX];
	char			*final_path;

	if (!lstat(args[2], &bufstruct) && S_ISLNK(bufstruct.st_mode))
	{
		ft_bzero(buf, PATH_MAX);
		readlink(args[2], buf, PATH_MAX);
		buf[ft_strlen(buf)] = '\0';
		final_path = cd_construct_path(buf);
		if (!final_path)
			final_path = ft_strjoin("/", buf);
		chdir(final_path);
		change_vars(all, args, final_path);
	}
	else
	{
		free(args[1]);
		args[1] = ft_strdup(args[2]);
		free(args[2]);
		args[2] = NULL;
		case_path(args, all);
	}
}
