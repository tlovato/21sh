/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_create_env.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/13 15:57:58 by tlovato           #+#    #+#             */
/*   Updated: 2016/12/13 16:00:08 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static char			*create_env_path(void)
{
	int				fd;
	int				retread;
	char			buf[1];
	char			*tmp;

	fd = open("/etc/paths", O_RDONLY);
	tmp = ft_strnew(1);
	while ((retread = read(fd, buf, 1)))
		tmp = ft_strjoinfree(tmp, (buf[0] == '\n') ? ":" : buf, 1);
	return (tmp);
}

void				create_env(t_env **envs)
{
	char			pwd[100];
	struct passwd	*pw;
	struct stat		st;

	getcwd(pwd, 100);
	stat(pwd, &st);
	pw = getpwuid(st.st_uid);
	if (((*envs)->tabenv = (char **)malloc(sizeof(char *) * 4)) == NULL)
		return ;
	(*envs)->tabenv[0] = ft_strjoin("PWD=", pwd);
	(*envs)->tabenv[1] = ft_strjoin("USER=", pw->pw_name);
	(*envs)->tabenv[2] = ft_strjoin("PATH=", create_env_path());
	(*envs)->tabenv[3] = NULL;
	(*envs)->lstenv = get_env_lst((*envs)->tabenv, 0);
}
