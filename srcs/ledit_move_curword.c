/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ledit_move_curword.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 16:05:09 by tlovato           #+#    #+#             */
/*   Updated: 2016/11/28 16:05:22 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static int		get_index(t_line *line, char *newstock, int left)
{
	int			car;

	car = line->cur;
	if (left && newstock[car] == '\0')
		car--;
	if (newstock[car] == ' ' || newstock[car] == '\t')
		while (newstock[car] == ' ' || newstock[car] == '\t')
			car = (left) ? car - 1 : car + 1;
	while (newstock[car]
		&& (newstock[car] != ' ' && newstock[car] != '\t'))
		car = (left) ? car - 1 : car + 1;
	if (car < 0)
		car = 0;
	return (car);
}

void			cur_word_right(t_line *line, char *newstock)
{
	int			car;

	car = get_index(line, newstock, 0);
	while (line->cur < car)
	{
		line->cur++;
		if (line->curinline == (line->ncol - 1))
		{
			ft_putstr("\033[1B");
			line->line += 1;
			while (line->curinline > 0)
			{
				ft_putstr("\033[1D");
				line->curinline--;
			}
		}
		else
		{
			ft_putstr("\033[1C");
			line->curinline += 1;
		}
	}
}

void			cur_word_left(t_line *line, char *newstock)
{
	int			car;

	car = get_index(line, newstock, 1);
	while (line->cur > car)
	{
		line->cur--;
		if (line->curinline == 0)
		{
			ft_putstr("\033[1A");
			line->line -= 1;
			while (line->curinline < (line->ncol - 1))
			{
				ft_putstr("\033[1C");
				line->curinline++;
			}
		}
		else
		{
			ft_putstr("\033[1D");
			line->curinline -= 1;
		}
	}
}
