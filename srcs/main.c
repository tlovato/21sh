/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/10 12:15:34 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/22 17:45:41 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

t_all				g_all;

int					ft_puts(int c)
{
	return (write(1, &c, 1));
}

void				do_cmd(t_all *all)
{
	t_token			*tmp;

	tmp = all->token;
	while (tmp)
	{
		if (tmp->cmd->args && tmp->cmd->args[0])
			pars_command(tmp->cmd, tmp->cmd->args, all);
		tmp = tmp->next;
	}
}

int					main(int ac, char **av, char **env)
{
	init_envs(&g_all.env, env);
	if (!init_term(&g_all))
		return (0);
	while (1)
	{
		g_all.ongoing = 0;
		handle_signals();
		g_all.path = get_path(g_all.env->lstenv);
		init_shell(&g_all);
		g_all.env->ntabenv = change_env(g_all.env->lstenv);
		do_cmd(&g_all);
		free_tokens(g_all.token);
		ft_free_str_tab(g_all.env->ntabenv);
	}
	close(g_all.fd);
	return (0);
}
