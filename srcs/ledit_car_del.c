/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ledit_car_del.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 16:38:16 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/20 16:59:11 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static void		suppr_car(char **newstock, int index)
{
	char		*tmp1;
	char		*tmp2;
	char		*tmp;

	tmp = *newstock;
	tmp1 = (char *)malloc(sizeof(char) * (index + 1));
	ft_strncpy(tmp1, *newstock, index);
	tmp1[index] = '\0';
	tmp2 = ft_strdup(&tmp[index + 1]);
	free(*newstock);
	*newstock = ft_strjoinfree(tmp1, tmp2, 1);
	tputs(tgetstr("ce", NULL), 1, ft_puts);
	ft_putstr_fd(tmp2, 1);
	tputs(tgetstr("ce", NULL), 1, ft_puts);
	index = 0;
	while (index < (int)ft_strlen(tmp2))
	{
		ft_putstr("\033[1D");
		index++;
	}
	free(tmp2);
}

static void		place_cur(t_line *line)
{
	if (line->curinline > (line->ncol - 1))
	{
		line->line += 1;
		line->curinline = 0;
	}
	else if (line->curinline <= 0)
	{
		line->line -= 1;
		line->curinline = (line->ncol - 1);
	}
	else
		line->curinline -= 1;
}

void			del_car(t_line *line, char **newstock)
{
	char		*checkquotes;

	checkquotes = *newstock;
	if (line->cur >= 1)
	{
		line->cur -= 1;
		if (checkquotes[line->cur] == '"' ||
			checkquotes[line->cur] == '\'' ||
			checkquotes[line->cur] == '`')
			line->quotes = 0;
		place_cur(line);
		ft_putstr("\033[1D");
		ft_putchar(127);
		ft_putstr("\033[1D");
		suppr_car(newstock, line->cur);
	}
}
