/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ledit_move_curline.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 16:53:27 by tlovato           #+#    #+#             */
/*   Updated: 2016/11/28 16:53:35 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

void		cur_line_left(t_line *line)
{
	while (line->curinline > 0 && line->cur > 0)
	{
		ft_putstr("\033[1D");
		line->curinline--;
		line->cur--;
	}
}

void		cur_line_right(t_line *line, char *newstock)
{
	while (line->curinline < (line->ncol - 1)
		&& line->cur < (int)ft_strlen(newstock))
	{
		ft_putstr("\033[1C");
		line->curinline++;
		line->cur++;
	}
}
