/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ledit_get_line.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 14:18:52 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/23 14:19:02 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static void		init_get_line(t_line *line, char *buf, int *q, t_all *all)
{
	ft_bzero(buf, 4);
	line->nlines = 1;
	line->line = 1;
	line->cur = 0;
	line->curinline = 3;
	all->term.c_lflag &= ~(ICANON);
	all->term.c_lflag &= ~(ECHO);
	all->term.c_cc[VMIN] = 1;
	all->term.c_cc[VTIME] = 0;
	tcsetattr(all->fd, TCSANOW, &all->term);
	line->newstock = ft_strnew(1);
	line->tabu = 0;
	line->quotes = 0;
	line->cpy = NULL;
	handle_signals();
	*q = 0;
}

static int		handle_ctld(int buf, t_line *line, int *heredoc, t_all *all)
{
	if (buf == 4 && line->cur == 0 && *heredoc == -1)
	{
		ft_putstr("exit\n");
		exit(EXIT_SUCCESS);
	}
	else if (buf == 4 && !(*heredoc))
		return ((*heredoc = 1));
	return (0);
}

static void		character(t_line *line, char *buf)
{
	if (*(int *)buf != 4)
	{
		if (line->curinline >= line->ncol)
		{
			line->nlines += 1;
			line->line += 1;
			line->curinline = 0;
		}
		pars_car_action(buf, &line->newstock, line);
	}
}

static void		ending_get_line(t_all *all)
{
	write(1, "\n", 1);
	all->term.c_lflag |= (ICANON);
	all->term.c_lflag |= (ECHO);
	tcsetattr(all->fd, TCSANOW, &all->init);
}

char			*get_line(t_line *line, t_ctrlhist *hist, t_all *all)
{
	char		buf[5];
	int			qbuf;
	int			retread;

	init_get_line(line, buf, &qbuf, all);
	while ((retread = read(0, buf, 4))
			&& (line->quotes) ? line->quotes : buf[0] != '\n')
	{
		if (handle_ctld(*(int *)buf, line, &all->heredoc, all))
			return (line->newstock);
		if (buf[0] == '\'' || buf[0] == '"' || buf[0] == '`')
			handle_quotes(line, &qbuf, *(int *)buf);
		copy_cut_paste(line, *(int *)buf, &line->newstock);
		if (retread == 1 && (buf[0] <= 127))
			character(line, buf);
		move_cursor(line->newstock, *(int *)buf, line);
		search_in_hist(*(int *)buf, hist, line);
		if (buf[0] == '\n' && line->quotes)
			val_cur_quotes(line);
		ft_bzero(buf, 4);
	}
	ending_get_line(all);
	if (line->cpy)
		free(line->cpy);
	return (line->newstock);
}
