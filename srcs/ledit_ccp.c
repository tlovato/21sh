/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ledit_ccp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 11:44:55 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/10 13:05:08 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

void			clean_line(t_line *line, char *newstock)
{
	int			car;
	char		*tmp;

	car = line->cur;
	tmp = ft_strdup(&newstock[line->cutstart]);
	while (car > line->cutstart)
	{
		ft_putstr("\033[1D");
		car--;
	}
	tputs(tgetstr("ce", NULL), 1, ft_puts);
	ft_putstr_fd(tmp, 1);
	car = ft_strlen(newstock);
	while (car > line->cur)
	{
		ft_putstr("\033[1D");
		car--;
	}
	free(tmp);
}

static void		displace_cur(t_line *line)
{
	while (line->cur > line->cutstart)
	{
		line->cur--;
		line->curinline--;
		ft_putstr("\033[1D");
		if (line->curinline == 0)
		{
			line->line--;
			line->curinline = line->ncol;
		}
	}
}

void			erase_part(t_line *line, char **newstock)
{
	char		*tmp;
	char		*tmp1;
	char		*tmp2;
	int			car;

	car = 0;
	tmp = *newstock;
	tmp1 = ft_strnew(line->cutstart);
	ft_strncpy(tmp1, *newstock, line->cutstart);
	tmp2 = ft_strdup(&tmp[line->cur]);
	displace_cur(line);
	tputs(tgetstr("ce", NULL), 1, ft_puts);
	free(*newstock);
	*newstock = ft_strjoinfree(tmp1, tmp2, 1);
	ft_putstr_fd(tmp2, 1);
	while (car < (int)ft_strlen(tmp2))
	{
		ft_putstr("\033[1D");
		car++;
	}
	if (tmp2)
		free(tmp2);
}

void			copy_cut_paste(t_line *line, int buf, char **newstock)
{
	if (buf == 8948194 || buf == 42947)
	{
		line->ccp = line->ccp ? 0 : 1;
		if (line->ccp)
			line->cutstart = line->cur;
		if (!line->ccp && buf == 8948194)
			erase_part(line, newstock);
		if (!line->ccp && buf == 42947)
			clean_line(line, *newstock);
		if (line->ccp)
		{
			if (line->cpy && line->cpy[0] != '\0')
				free(line->cpy);
			line->cpy = ft_strnew(1);
		}
	}
	if (buf == 10127586 && line->cpy)
		paste_cpy(line, newstock);
}
