/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_get_path.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/12 10:48:49 by tlovato           #+#    #+#             */
/*   Updated: 2016/10/12 10:48:58 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

int					find_equal(char *arg)
{
	int				i;

	i = 0;
	while (arg[i] != '=' && arg[i])
		i++;
	if (arg[i] == '\0')
		i = 0;
	return (i);
}

char				*get_path(t_lstenv *menv)
{
	int				i;
	char			*sub;
	t_lstenv		*tmp;

	i = 0;
	tmp = menv;
	while (tmp)
	{
		i = find_equal(tmp->line) + 1;
		sub = ft_strsub(tmp->line, 0, i);
		if (!ft_strcmp(sub, "PATH="))
		{
			free(sub);
			return (&tmp->line[5]);
		}
		if (sub)
			free(sub);
		tmp = tmp->next;
	}
	return (NULL);
}
