/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redir_agreg_fd.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/13 13:35:23 by tlovato           #+#    #+#             */
/*   Updated: 2016/12/13 13:35:38 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static int		get_number_fd(char *redir, int *next_fd)
{
	char		*tmp;
	int			i;

	i = 0;
	while (redir[i] && ft_isdigit(redir[i]))
		i++;
	*next_fd = i;
	while (redir[*next_fd] && !ft_isdigit(redir[*next_fd]))
		(*next_fd)++;
	tmp = ft_strsub(redir, 0, i);
	i = ft_atoi(tmp);
	free(tmp);
	return (i);
}

int				get_redir_fd(char *redir, int *next_fd)
{
	if (ft_isdigit(redir[0]))
		return (get_number_fd(redir, next_fd));
	else
		return ((redir[0] == '>') ? 1 : -1);
}
