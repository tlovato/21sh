/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexpa_spe_car_substitution.c                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 15:59:13 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/23 15:59:23 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static char		*tild_substitution(char *ret)
{
	char		*start;
	char		*end;
	int			i;
	char		*home;

	start = NULL;
	end = NULL;
	i = 0;
	home = getenv("HOME");
	while (ret[i])
	{
		if (ret[i] == '~')
		{
			start = ft_strsub(ret, 0, i);
			end = ft_strsub(ret, i + 1, ft_strlen(ret));
			free(ret);
			ret = ft_strjoinfree(start, home, 1);
			ret = ft_strjoinfree(ret, end, 3);
		}
		i++;
	}
	return (ret);
}

char			*spe_car_substitution(int quotes, char *tmpline, t_all *all)
{
	char		*ret;

	ret = ft_strdup(tmpline);
	if (quotes != '\'')
	{
		ret = var_substitution(ret);
		ret = bquotes(ret, all, quotes);
		if (!quotes)
			ret = tild_substitution(ret);
	}
	return (ret);
}
