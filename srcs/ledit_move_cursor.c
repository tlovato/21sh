/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ledit_move_cursor.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/02 16:39:22 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/10 16:38:22 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static void		cur_up(t_line *line)
{
	if (line->line > 1)
	{
		line->line -= 1;
		line->cur = line->cur - line->ncol;
		ft_putstr("\033[1A");
	}
}

static void		cur_down(t_line *line, char *newstock)
{
	int			cur;

	cur = line->cur + line->ncol;
	if ((line->line <= line->nlines) && (cur < (int)ft_strlen(newstock)))
	{
		line->line += 1;
		line->cur = cur;
		ft_putstr("\033[1B");
	}
}

void			move_cursor(char *newstock, int buf, t_line *line)
{
	if (buf == 4479771)
		cur_left(line, newstock);
	if (buf == 4414235)
		cur_right(line, newstock);
	if (buf == 1113266971)
		cur_down(line, newstock);
	if (buf == 1096489755)
		cur_up(line);
	if (buf == 1130044187)
		cur_word_right(line, newstock);
	if (buf == 1146821403)
		cur_word_left(line, newstock);
	if (buf == 4741915)
		cur_line_left(line);
	if (buf == 4610843)
		cur_line_right(line, newstock);
}
