/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexpa_parser.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/11 10:41:08 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/22 17:43:19 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

t_token			*parser(char *tmp, t_token **token, t_all *all)
{
	char		**seps;

	seps = ft_strsplit(tmp, ';');
	*token = get_tokens(seps, 0, all);
	ft_free_str_tab(seps);
	return (*token);
}
