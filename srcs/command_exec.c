/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command_exec.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/12 11:45:21 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/19 18:10:44 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static char			*find_access(char *tmptab, char *cmd)
{
	char			*tmp;

	tmp = ft_strdup(tmptab);
	tmp = ft_strjoinfree(tmp, "/", 1);
	tmp = ft_strjoinfree(tmp, cmd, 1);
	return ((access(tmp, X_OK)) ? NULL : ft_strdup(tmp));
}

char				*find_in_path(char *cmd, t_all *all)
{
	char			*here;
	char			**tmptab;
	int				i;
	DIR				*dirp;
	struct dirent	*dp;

	tmptab = ft_strsplit(all->path, ':');
	i = 0;
	here = NULL;
	while (tmptab[i])
	{
		if ((dirp = opendir(tmptab[i])))
		{
			while ((dp = readdir(dirp)))
				if (!ft_strcmp(dp->d_name, cmd))
					here = find_access(tmptab[i], cmd);
			closedir(dirp);
		}
		i++;
	}
	ft_free_str_tab(tmptab);
	return (here);
}

void				execution(char **args, t_all *all, int path)
{
	char			*here;

	here = NULL;
	if (path)
	{
		if (all->path)
			here = find_in_path(args[0], all);
		if (!here)
		{
			ft_printf("21sh: Command not found : ");
			ft_putendl((args[0]) ? args[0] : "\n");
			exit(EXIT_FAILURE);
		}
	}
	else if (access(args[0], X_OK))
	{
		ft_printf("21sh: No such file or directory: ");
		ft_putendl(args[0]);
		exit(EXIT_FAILURE);
	}
	execve((here) ? here : args[0], args, all->env->ntabenv);
	if (here)
		free(here);
}
