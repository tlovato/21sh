/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redirs.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/27 12:13:04 by tlovato           #+#    #+#             */
/*   Updated: 2016/10/27 12:13:36 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

void			redir_in(t_cmd *cmd, t_redir *redir, t_all *all)
{
	int			newfd;

	if ((newfd = open(redir->file_name, O_RDWR)) == -1)
	{
		ft_putstr("21sh: ");
		ft_putstr(redir->file_name);
		ft_putendl(": No such file or directory");
		exit(EXIT_FAILURE);
	}
	dup2(newfd, STDIN_FILENO);
}
