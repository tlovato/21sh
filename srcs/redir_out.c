/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redir_out.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 12:37:21 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/17 20:16:06 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static char		*read_out(int fd_out)
{
	char		buf[2];
	char		*newstock;
	char		*cat;
	char		*ret;

	newstock = ft_strnew(1);
	while (read(fd_out, buf, 1))
	{
		cat = ft_strnew(1);
		buf[1] = '\0';
		ft_strcpy(cat, buf);
		newstock = ft_strjoinfree(newstock, cat, 3);
	}
	return (newstock);
}

static int		create_fd(t_redir *redir, int newfd)
{
	char		buf[1];

	if (!ft_strcmp(redir->redir, ">"))
	{
		if ((newfd = open(redir->file_name, O_RDWR | O_TRUNC)) == -1)
			newfd = open(redir->file_name, O_CREAT | O_RDWR | O_TRUNC, 0644);
	}
	else
	{
		if ((newfd = open(redir->file_name, O_RDWR)) == -1)
			newfd = open(redir->file_name, O_CREAT | O_RDWR, 0644);
		while (read(newfd, buf, 1))
			;
	}
	return (newfd);
}

void			redir_out(t_cmd *cmd, t_redir *redir, t_all *all)
{
	int			newfd;
	char		*out;

	if (!opendir(redir->file_name))
	{
		newfd = create_fd(redir, newfd);
		dup2(newfd, STDOUT_FILENO);
		out = read_out(STDOUT_FILENO);
		write(newfd, out, ft_strlen(out));
		close(newfd);
		free(out);
	}
	else
	{
		ft_putstr("21sh: ");
		ft_putstr(redir->file_name);
		ft_putendl(": Is a directory");
	}
}
