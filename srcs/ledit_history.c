/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ledit_history.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/15 12:41:00 by tlovato           #+#    #+#             */
/*   Updated: 2016/11/15 12:41:09 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

void			add_to_history(char *newstock, t_ctrlhist *ctrlhist)
{
	t_hist		*new;

	if (!(new = (t_hist *)malloc(sizeof(t_hist))))
		return ;
	new->cmd = ft_strdup(newstock);
	new->next = NULL;
	if (!ctrlhist->tail)
	{
		new->prev = NULL;
		ctrlhist->head = new;
		ctrlhist->tail = new;
	}
	else
	{
		ctrlhist->tail->next = new;
		new->prev = ctrlhist->tail;
		ctrlhist->tail = new;
	}
	ctrlhist->len++;
}

static void		line_sub(t_line *line, char *tmpcmd)
{
	while (line->cur > 0)
	{
		ft_putstr("\033[1D");
		tputs(tgetstr("ce", NULL), 1, ft_puts);
		line->cur--;
		line->curinline--;
		if (line->curinline == 0 && line->line > 1)
		{
			tputs(tgetstr("ce", NULL), 1, ft_puts);
			line->line--;
			line->curinline = line->ncol - 1;
		}
	}
	ft_putstr(tmpcmd);
}

static void		replace_cur(t_line *line, char *tmpcmd)
{
	while (line->cur < (int)(ft_strlen(tmpcmd)))
	{
		line->curinline++;
		line->cur++;
		if (line->curinline == line->ncol)
		{
			line->line++;
			line->nlines++;
			line->curinline = 0;
		}
	}
}

static void		replace_line(t_line *line, char *tmpcmd, char **newstock)
{
	line_sub(line, tmpcmd);
	replace_cur(line, tmpcmd);
	if (*newstock && tmpcmd)
	{
		if (newstock)
			free(*newstock);
		*newstock = ft_strdup(tmpcmd);
	}
}

void			search_in_hist(int buf, t_ctrlhist *hist, t_line *line)
{
	char		*tmpcmd;

	tmpcmd = NULL;
	if (hist->head && hist->tail
		&& (buf == 4283163 || buf == 4348699))
	{
		if (buf == 4283163)
		{
			tmpcmd = hist->tail->cmd;
			if (hist->tail->prev)
				hist->tail = hist->tail->prev;
		}
		else if (buf == 4348699)
		{
			if (hist->tail->next)
			{
				tmpcmd = hist->tail->next->cmd;
				hist->tail = hist->tail->next;
			}
			else if (!hist->tail->next)
				tmpcmd = "\0";
		}
		if (tmpcmd)
			replace_line(line, tmpcmd, &line->newstock);
	}
}
