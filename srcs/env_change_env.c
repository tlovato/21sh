/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_change_env.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/11 18:12:13 by tlovato           #+#    #+#             */
/*   Updated: 2016/10/11 18:12:23 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

char			**cpy_env(char **env)
{
	int			len;
	char		**ret;
	int			i;

	i = 0;
	len = ft_strtablen(env);
	ret = (char **)malloc(sizeof(char *) * (len + 1));
	while (i < len)
	{
		ret[i] = ft_strdup(env[i]);
		i++;
	}
	ret[i] = NULL;
	return (ret);
}

t_lstenv		*get_env_lst(char **env, int i)
{
	t_lstenv	*new;

	if (i < ft_strtablen(env))
	{
		new = (t_lstenv *)malloc(sizeof(t_lstenv));
		new->line = ft_strdup(env[i]);
		i += 1;
		new->next = get_env_lst(env, i);
		return (new);
	}
	return (NULL);
}

static int		calc_len(t_lstenv *menv)
{
	t_lstenv	*tmp;
	int			ret;

	tmp = menv;
	ret = 0;
	while (tmp)
	{
		tmp = tmp->next;
		ret++;
	}
	return (ret);
}

char			**change_env(t_lstenv *menv)
{
	char		**ret;
	t_lstenv	*tmp;
	int			i;

	i = 0;
	tmp = menv;
	ret = (char **)malloc(sizeof(char *) * (calc_len(tmp) + 1));
	while (tmp)
	{
		ret[i] = ft_strdup(tmp->line);
		tmp = tmp->next;
		i++;
	}
	ret[i] = NULL;
	return (ret);
}
