/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_ft_env.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/12 13:13:46 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/11 18:42:04 by lpoujade         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static char		**env_add(char *keyval, char **env)
{
	char		**new;
	int			t;
	int			i;

	i = 0;
	t = (env) ? ft_strtablen(env) : 0;
	if (!(new = (char **)malloc(sizeof(char *) * (t + 2))))
		return (NULL);
	while (env[i])
	{
		if (!ft_strncmp(env[i], keyval, find_equal(keyval)))
			break ;
		new[i] = ft_strdup(env[i]);
		i++;
	}
	new[i] = ft_strdup(keyval);
	i++;
	while (i < t && env[i])
	{
		new[i] = ft_strdup(env[i]);
		i++;
	}
	new[i] = NULL;
	return (new);
}

static void		display(char **new_env)
{
	int			i;

	i = 0;
	if (new_env)
		while (new_env[i])
			ft_putendl(new_env[i++]);
}

void			ft_env(t_cmd *cmd, char **args, t_all *all)
{
	char		**new_env;
	int			i;

	i = 1;
	new_env = NULL;
	if (!ft_strcmp(args[1], "-i"))
	{
		new_env = (char**)malloc(sizeof(char*));
		new_env[0] = NULL;
		i++;
	}
	else
		new_env = cpy_env(all->env->ntabenv);
	while (args[i] && ft_strchr(args[i], '='))
		new_env = env_add(args[i++], new_env);
	ft_free_str_tab(all->env->ntabenv);
	all->env->ntabenv = cpy_env(new_env);
	if (args[i])
		pars_command(cmd, &args[i], all);
	else
		display(all->env->ntabenv);
	if (new_env)
		ft_free_str_tab(new_env);
}
