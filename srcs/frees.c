/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   frees.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/11 17:58:50 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/11 01:16:17 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

void			free_lstenv(t_lstenv *env)
{
	t_lstenv	*tmpenv;

	while (env)
	{
		tmpenv = env->next;
		if (env->line)
			free(env->line);
		free(env);
		env = tmpenv;
	}
	free(env);
	env = NULL;
}

static void		free_redir(t_redir *redir)
{
	t_redir		*tmpredir;

	while (redir)
	{
		tmpredir = redir->next;
		if (redir->redir)
			free(redir->redir);
		if (redir->file_name)
			free(redir->file_name);
		free(redir);
		redir = tmpredir;
	}
	free(redir);
	redir = NULL;
}

void			free_cmd(t_cmd *cmd)
{
	t_cmd		*tmpcmd;

	while (cmd)
	{
		tmpcmd = cmd->next;
		if (cmd->args)
			ft_free_str_tab(cmd->args);
		if (cmd->redir)
			free_redir(cmd->redir);
		free(cmd);
		cmd = tmpcmd;
	}
	free(cmd);
	cmd = NULL;
}

void			free_tokens(t_token *token)
{
	t_token		*tmptok;

	while (token)
	{
		tmptok = token->next;
		if (token->cmd)
			free_cmd(token->cmd);
		free(token);
		token = tmptok;
	}
	free(token);
	token = NULL;
}
