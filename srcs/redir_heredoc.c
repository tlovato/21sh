/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redir_heredoc.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 12:33:15 by tlovato           #+#    #+#             */
/*   Updated: 2016/11/07 12:33:17 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static char		*take_heredoc(t_cmd *cmd, t_all *all)
{
	char		*tmp;
	char		*tmp2;
	char		*ret;
	t_line		new;

	new.ccp = 0;
	new.cutstart = 0;
	new.left_margin = 3;
	new.ncol = tgetnum("co");
	tmp = NULL;
	ret = ft_strnew(1);
	all->heredoc = 0;
	while (ft_strcmp(tmp, cmd->redir->file_name) && all->heredoc != 1)
	{
		ft_putstr("> ");
		tmp = get_line(&new, &all->ctrlhist, all);
		tmp2 = ft_strjoin(tmp, "\n");
		if (ft_strcmp(tmp, cmd->redir->file_name))
			ret = ft_strjoinfree(ret, tmp2, 1);
		free(tmp2);
	}
	if (all->heredoc == 1)
		ft_putstr("\n");
	return (ret);
}

void			redir_heredoc(t_cmd *cmd, t_all *all)
{
	char		*heredoc;
	int			pipefd[2];
	pid_t		pid;

	heredoc = take_heredoc(cmd, all);
	pipe(pipefd);
	pid = fork();
	if (pid == 0)
	{
		dup2(pipefd[1], STDOUT_FILENO);
		close(pipefd[0]);
		write(STDOUT_FILENO, heredoc, ft_strlen(heredoc));
		free(heredoc);
		exit(EXIT_SUCCESS);
	}
	dup2(pipefd[0], STDIN_FILENO);
	close(pipefd[1]);
	wait(NULL);
}
