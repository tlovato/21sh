/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_cd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/14 12:13:15 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/19 22:04:26 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static void		case_home(t_all *all)
{
	char		*home;
	char		*pwd;
	char		*oldpwd;

	home = cd_find_var("HOME", all);
	pwd = cd_find_var("PWD", all);
	if (!home)
	{
		ft_putstr("21sh: cd: No home directory\n");
		return ;
	}
	else
	{
		if (chdir(&home[5]) < 0)
		{
			ft_cd_errors(2, home);
			return ;
		}
		if (pwd)
			cd_change_vars("PWD=", &home[5], all);
		if ((oldpwd = cd_find_var("OLDPWD", all)) && pwd)
			cd_change_vars("OLDPWD=", &pwd[4], all);
	}
	cd_free_str(home, pwd, oldpwd);
}

static void		case_oldpwd(t_all *all)
{
	char		*oldpwd;
	char		*pwd;

	oldpwd = cd_find_var("OLDPWD", all);
	pwd = cd_find_var("PWD", all);
	if (!oldpwd)
	{
		ft_putstr("21sh: cd: No OLDPWD variable set\n");
		return ;
	}
	else
	{
		if (chdir(&oldpwd[7]) < 0)
		{
			ft_cd_errors(2, &oldpwd[7]);
			return ;
		}
		if (pwd)
			cd_change_vars("PWD=", &oldpwd[7], all);
		if (oldpwd && pwd)
			cd_change_vars("OLDPWD=", &pwd[4], all);
	}
	cd_free_str(oldpwd, pwd, NULL);
}

void			case_path(char **args, t_all *all)
{
	char		*pwd;
	char		*oldpwd;
	char		buf[100];
	char		*finaldir;

	finaldir = cd_construct_path(args[1]);
	if (chdir(finaldir) < 0)
	{
		ft_cd_errors(2, args[1]);
		free(finaldir);
		return ;
	}
	pwd = cd_find_var("PWD", all);
	oldpwd = cd_find_var("OLDPWD", all);
	if (pwd && oldpwd)
	{
		cd_change_vars("OLDPWD=", &pwd[4], all);
		cd_change_vars("PWD=", getcwd(buf, 100), all);
	}
	cd_free_str(pwd, oldpwd, finaldir);
}

void			ft_cd(char **args, t_all *all)
{
	int			len;
	int			valid_option;

	len = ft_strtablen(args);
	valid_option = (!ft_strcmp(args[1], "-L") || !ft_strcmp(args[1], "-P"));
	if (len > 2 && !valid_option)
		ft_cd_errors(1, args[1]);
	else
	{
		if (len == 1 || (len == 2 && valid_option))
			case_home(all);
		else if (!ft_strcmp(args[1], "-"))
			case_oldpwd(all);
		else if (len == 3 && valid_option)
			case_option(args, all);
		else
			case_path(args, all);
	}
}
