/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexpa_split_cmd.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 16:43:56 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/23 16:44:04 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static char		*take_cmd(int *val, char *line, int *quotes, t_all *all)
{
	char		*tmpline;
	char		*ret;
	char		*ptrline;

	val[2] = 0;
	ptrline = &line[val[1]];
	tmpline = NULL;
	val[2] = count_wlen(&ptrline, &val[1], quotes);
	tmpline = ft_strsub(ptrline, 0, val[2]);
	ret = spe_car_substitution(*quotes, tmpline, all);
	val[1] = val[1] + val[2];
	if (tmpline)
		free(tmpline);
	return (ret);
}

char			**split_cmd(char *line, t_all *all)
{
	char		**tmp;
	int			val[3];
	int			quotes;

	val[0] = 0;
	val[1] = 0;
	tmp = NULL;
	if (line)
	{
		tmp = (char **)malloc(sizeof(char *) * (count_words(line) + 1));
		while (val[0] < count_words(line))
		{
			quotes = 0;
			while (line[val[1]] == ' '
				|| line[val[1]] == '\t' || line[val[1]] == '\n')
				val[1]++;
			tmp[val[0]] = take_cmd(val, line, &quotes, all);
			if (quotes)
				val[1] += 1;
			val[0]++;
		}
		tmp[val[0]] = NULL;
	}
	return (tmp);
}
