/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signals.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 17:32:20 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/11 22:21:46 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

extern t_all	g_all;

static void		pars_signal(int sig)
{
	if (sig == SIGINT)
	{
		if (g_all.line.quotes != 0)
			g_all.line.quotes = 0;
		if (g_all.ongoing != 1)
			ft_putstr("\n$> ");
		else if (g_all.ongoing == 1)
			ft_putchar('\n');
		ft_bzero(g_all.line.newstock, ft_strlen(g_all.line.newstock));
	}
}

void			handle_signals(void)
{
	int			sig;

	sig = 0;
	while (sig < 32)
	{
		if (sig != SIGQUIT && sig != SIGSEGV && sig != SIGBUS && sig != SIGTRAP
				&& sig != SIGILL && sig != SIGABRT && sig != SIGEMT &&
				sig != SIGFPE && sig != SIGSYS)
			signal(sig, &pars_signal);
		sig++;
	}
}
