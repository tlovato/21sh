/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipes.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/27 12:02:34 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/19 17:04:13 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

void			communication(t_cmd *cmd, t_all *all)
{
	int			pipefd[2];
	pid_t		pid;

	pipe(pipefd);
	pid = fork();
	if (pid == 0)
	{
		close(pipefd[0]);
		dup2(pipefd[1], STDOUT_FILENO);
		if (cmd->redir && cmd->redir->fd1 == -1
			&& ft_strcmp(cmd->redir->redir, "<<"))
			pars_redir(cmd, cmd->redir, all);
		do_exec(cmd, cmd->args, all);
	}
	else
	{
		close(pipefd[1]);
		dup2(pipefd[0], STDIN_FILENO);
		if (cmd->next->next)
			communication(cmd->next, all);
		if (cmd->next->redir && cmd->next->redir->fd1 == -1
			&& ft_strcmp(cmd->next->redir->redir, "<<"))
			pars_redir(cmd->next, cmd->next->redir, all);
		do_exec(cmd->next, cmd->next->args, all);
	}
}

static void		pipe_error(t_cmd *cmd, int message, int *ret)
{
	if (!message)
		ft_putstr("21sh: Command not found: ");
	else
		ft_putstr("21sh: No such file or directory: ");
	ft_putendl(cmd->args[0]);
	*ret = 0;
}

int				valid(t_cmd *cmd, t_all *all)
{
	t_cmd		*tmp;
	char		*here;
	int			ret;

	tmp = cmd;
	ret = 1;
	while (tmp)
	{
		if (!(here = find_in_path(tmp->args[0], all))
			&& tmp->args[0][0] != '/')
			pipe_error(tmp, 0, &ret);
		else if (tmp->args[0][0] == '/' && access(tmp->args[0], X_OK))
			pipe_error(tmp, 1, &ret);
		if (!ret)
			return (ret);
		tmp = tmp->next;
	}
	if (here)
		free(here);
	return (ret);
}
