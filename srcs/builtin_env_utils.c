/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_env_utils.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 12:38:01 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/23 12:38:03 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

int				var_exists(char *arg, t_all *all)
{
	char		*sub;
	t_lstenv	*tmp;
	int			equal;

	tmp = all->env->lstenv;
	equal = find_equal(arg) + 1;
	sub = ft_strsub(arg, 0, equal);
	while (tmp)
	{
		if (ft_strstr(tmp->line, sub))
		{
			free(sub);
			ft_putstr(tmp->line);
			ft_putendl(&arg[equal]);
			return (1);
		}
		tmp = tmp->next;
	}
	if (sub)
		free(sub);
	return (0);
}

void			display_env(t_all *all)
{
	t_lstenv	*tmp;

	tmp = all->env->lstenv;
	while (tmp)
	{
		ft_putendl(tmp->line);
		tmp = tmp->next;
	}
}

int				valid_arg(char **args)
{
	int			i;

	i = 1;
	while (args[i])
	{
		if (!ft_strrchr(args[i], '='))
			return (i);
		i++;
	}
	return (0);
}
