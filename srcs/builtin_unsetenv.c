/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_unsetenv.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/12 15:29:16 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/22 13:23:58 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/sh21.h"

static void		erase_var(t_lstenv **lstenv, char *var)
{
	t_lstenv	*cur;
	t_lstenv	*tmp;

	cur = *lstenv;
	tmp = *lstenv;
	while (tmp && ft_strcmp(tmp->line, var))
	{
		cur = tmp;
		tmp = tmp->next;
	}
	if (cur == tmp)
		*lstenv = (*lstenv)->next;
	else if (tmp)
		cur->next = tmp->next;
	free(tmp->line);
	free(tmp);
	tmp = NULL;
}

static int		sub_and_cmp(char *line, char *tmp, t_all *all)
{
	char		*sub;

	if (line)
	{
		sub = ft_strsub(line, 0, ft_strlen(tmp));
		if (!ft_strcmp(sub, tmp))
		{
			erase_var(&all->env->lstenv, line);
			free(sub);
			return (1);
		}
	}
	free(sub);
	return (0);
}

void			ft_unsetenv(char **args, t_all *all)
{
	int			i;
	char		*tmp;
	t_lstenv	*tmpenv;

	i = 1;
	if (ft_strtablen(args) == 1)
		return ;
	while (args[i])
	{
		tmpenv = all->env->lstenv;
		tmp = ft_strjoin(args[i], "=");
		while (tmpenv)
		{
			if (sub_and_cmp(tmpenv->line, tmp, all))
				break ;
			tmpenv = tmpenv->next;
		}
		free(tmp);
		i++;
	}
}
