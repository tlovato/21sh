/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   21sh.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/10 12:16:16 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/21 17:36:30 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SH21_H
# define SH21_H

# include "libft.h"
# include "libftprintf.h"
# include <signal.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <sys/wait.h>
# include <dirent.h>
# include <termios.h>
# include <term.h>
# include <curses.h>
# include <errno.h>
# include <pwd.h>
# include <limits.h>

/*
**	handling env
*/
typedef struct		s_lstenv
{
	char			*line;
	struct s_lstenv	*next;
}					t_lstenv;

typedef struct		s_env
{
	t_lstenv		*lstenv;
	char			**tabenv;
	char			**ntabenv;
}					t_env;

/*
**	handling redir
*/

typedef struct		s_redir
{
	char			*redir;
	char			*file_name;
	int				fd1;
	int				fd2;
	struct s_redir	*next;
}					t_redir;

/*
**	handling cmds
*/

typedef struct		s_cmd
{
	char			**args;
	int				pipe;
	struct s_redir	*redir;
	struct s_cmd	*next;
}					t_cmd;

typedef struct		s_token
{
	struct s_cmd	*cmd;
	struct s_token	*next;
}					t_token;

/*
**	handling line
*/

typedef struct		s_line
{
	int				left_margin;
	int				right_margin;
	int				cur;
	int				curinline;
	int				line;
	int				nlines;
	int				ncol;
	char			*cpy;
	int				ccp;
	int				cutstart;
	int				tabu;
	int				quotes;
	char			*newstock;
}					t_line;

/*
**	handling historic
*/
typedef struct		s_hist
{
	char			*cmd;
	struct s_hist	*next;
	struct s_hist	*prev;
}					t_hist;

typedef struct		s_ctrlhist
{
	int				len;
	t_hist			*tail;
	t_hist			*head;
}					t_ctrlhist;

/*
**	all struct
*/

typedef struct		s_all
{
	t_env			*env;
	t_token			*token;
	t_ctrlhist		ctrlhist;
	t_ctrlhist		tmpctrlhist;
	t_line			line;
	char			*path;
	int				fd;
	struct termios	term;
	struct termios	init;
	int				heredoc;
	pid_t			pid;
	int				ongoing;
	int				redir;
}					t_all;

/*
** À AJOUTER À LA LIB : (tous déjà dans "../libft/")
*/
char				*ft_strndup(char const *str, size_t len);
char				*ft_strrmstr(char *str, char *torm);
char				**ft_cpy_tab(char **chartab);

/*
**	INITIALISATION : init.c
*/
void				init_shell(t_all *all);
void				init_envs(t_env **envs, char **env);
int					init_term(t_all *all);
void				init_values(t_all *all);
void				new_line_val(t_all *all);

/*
**	FREE ALLOCATIONS : frees.c
*/
void				free_lstenv(t_lstenv *env);
void				free_tokens(t_token *token);
void				free_cmd(t_cmd *cmd);

/*
**	ENV (RECUPERATION, MODIFICATION & PATH) : env_change_env.c env_get_path.c\
**	env_create_env.c
*/
t_lstenv			*get_env_lst(char **env, int i);
char				**cpy_env(char **env);
char				**change_env(t_lstenv *menv);
char				*get_path(t_lstenv *menv);
int					find_equal(char *arg);
void				create_env(t_env **envs);

/*
**	LEXER - PARSER : lexpa_parser.c lexpa_utils_parser.c lexpa_split_cmd.c\
**	lexpa_spe_car_substitution.c
*/
t_token				*parser(char *tmp, t_token **token, t_all *all);
t_token				*get_tokens(char **seps, int i, t_all *all);
t_cmd				*get_cmd(char *token, int max, int i, t_all *all);
int					count_cmds(char *token);
char				**split_cmd(char *line, t_all *all);
char				*spe_car_substitution(int quotes, char *line, t_all *all);
char				**get_args(char **tmp);
t_redir				*get_redir(char **tmp, int i, int max, int st);
char				*var_substitution(char *tmpline);
char				*bquotes(char *tmpline, t_all *all, int quotes);
int					count_wlen(char **line, int *k, int *quotes);
int					count_words(char *line);

/*
**	LES BUILTINS : builtin_ft_cd.c builtin_ft_cd_utils.c builtin_ft_exit.c\
**	builtin_ft_echo.c builtin_ft_env. builtin_ft_setenv.c builtin_ft_unsetenv.c\
**	builtin_env_utils.c
*/
void				ft_exit(char **args, t_all *all);
void				cd_free_str(char *s1, char *s2, char *s3);
void				cd_change_vars(char *tochange, char *newval, t_all *all);
char				*cd_find_var(char *tofind, t_all *all);
char				*cd_construct_path(char *av);
void				ft_cd_errors(int err, char *arg);
void				ft_cd(char **args, t_all *all);
void				ft_echo(char **args, t_all *all);
void				ft_env(t_cmd *cmd, char **args, t_all *all);
void				display_env(t_all *all);
void				ft_setenv(char **args, t_all *all);
void				ft_unsetenv(char **args, t_all *all);
void				case_option(char **args, t_all *all);
void				case_path(char **args, t_all *all);
int					valid_arg(char **args);
int					var_exists(char *arg, t_all *all);

/*
** PARSING DES COMMANDES : command_parsing.c main.c
*/
void				pars_command(t_cmd *cmd, char **args, t_all *all);
void				pars_redir(t_cmd *cmd, t_redir *redir, t_all *all);
void				do_cmd(t_all *all);
void				execution(char **args, t_all *all, int path);

/*
**	EXECUTION DES COMMANDES : command_exec.c
*/
void				exec_binary(char **args, t_all *all);
void				exec_with_path(char **args, t_all *all);
char				*find_in_path(char *cmd, t_all *all);
void				do_exec(t_cmd *cmd, char **args, t_all *all);

/*
**	PIPES : pipes.c
*/
void				communication(t_cmd *cmd, t_all *all);
int					valid(t_cmd *cmd, t_all *all);

/*
**	REDIRS : redir_heredoc.c redir_out.c redir_in.c redir_agreg_fd.c
*/
void				redir_heredoc(t_cmd *cmd, t_all *all);
void				redir_out(t_cmd *cmd, t_redir *redir, t_all *all);
void				redir_in(t_cmd *cmd, t_redir *redir, t_all *all);
int					get_redir_fd(char *redir, int *next);

/*
**	LINE EDITION : main.c ledit_move_cursor.c ledit_get_line.c ledit_car_add.c
**	ledit_car_del.c ledit_move_curword.c ledit_move_curline.c
*/
int					ft_puts(int c);
void				move_cursor(char *newstock, int buf, t_line *line);
void				del_car(t_line *line, char **newstock);
char				*get_line(t_line *line, t_ctrlhist *hist, t_all *all);
void				add_car(char *buf, char **newstock, t_line *line);
void				cur_word_right(t_line *line, char *newstock);
void				cur_word_left(t_line *line, char *newstock);
void				cur_line_left(t_line *line);
void				cur_line_right(t_line *line, char *newstock);
void				pars_car_action(char *buf, char **newstock, t_line *line);
void				cur_right(t_line *line, char *newstock);
void				cur_left(t_line *line, char *newstock);
void				end_get_line(t_all *all);
void				handle_quotes(t_line *line, int *qbuf, int buf);
void				val_cur_quotes(t_line *line);

/*
**	HISTORY : ledit_history.c
*/
void				add_to_history(char *newstock, t_ctrlhist *ctrlhist);
void				search_in_hist(int buf, t_ctrlhist *hist, t_line *line);

/*
**	COPY/CUT - PASTE : ledit_ccp.c ledit_cut.c ledit_copy.c ledit_paste.c
*/
void				copy_cut_paste(t_line *line, int buf, char **newstock);
void				erase_part(t_line *line, char **newstock);
void				clean_line(t_line *line, char *newstock);
void				paste_cpy(t_line *line, char **newstock);

/*
**	SIGNALS : signals.c
*/
void				handle_signals(void);

#endif
